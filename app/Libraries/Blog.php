<?php

namespace App\Libraries;

class Blog {
    public function printItem($param) {
        return view('components/post_item', $param);
    }
}