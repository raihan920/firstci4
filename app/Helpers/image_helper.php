<?php

function createProfileImageName($fileName, $directory = 'default'){
    $path = './uploads/profile-picture/';

    if ($directory!='default') {
        $path = $path.$directory.'/';
        //if directory doesnot exist create it
        if(!file_exists($path)){
            mkdir($path, 755);
        }
    }
    return $path.$fileName;
}
?>