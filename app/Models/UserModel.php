<?php namespace App\Models;

use App\Libraries\Utility;
use CodeIgniter\Model;

class UserModel extends Model
{
    protected $table      = 'users';
    protected $primaryKey = 'user_id';

    // protected $returnType     = 'array';
    // protected $useSoftDeletes = true;

    protected $allowedFields = ['name','email','password','category','date_of_birth','profile_picture','profile_picture_thumbs'];

    protected $useTimestamps = true;
    protected $createdField  = 'user_created_at';
    protected $updatedField  = 'user_updated_at';
    // protected $deletedField  = 'deleted_at';

    // protected $validationRules    = [];
    // protected $validationMessages = [];
    // protected $skipValidation     = false;

    protected $beforeInsert = ['hashPassword']; //codeigniter will check this methodds before insert
        
    public function checkTitle(array $data)
    {
        $newTitle = $data['data']['post_title'].' Blog Post Title';
        $data['data']['post_title'] = $newTitle;
        return $data;
    }
    
    public function getUsers(){
        $builder = $this->table($this->table);
        $users = $builder->get()->getResult();
        return $users;
    }

    public function hashPassword(array $data)
    {
        $passwordStr = $data['data']['password'];        
        //$match = preg_match("/(?=.*[\d])(?=.*[A-Z])(?=.*[a-z])(?=.*[!\"#$%&\'()*+,-.\/:;<=>?@[\]^_`{|}~])[\w\d!\"#$%&\'()*+,-.\/:;<=>?@[\]^_`{|}~]/", $passwordStr);

        if (isset($passwordStr))
        {
            // $data['data']['password'] = password_hash($passwordStr, PASSWORD_DEFAULT);
            $data['data']['password'] = md5($passwordStr); //md5 password hash
            return $data;
        }
        else
        {            
            unset($data); 
            return $data;
        }
        
    }
}