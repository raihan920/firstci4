<?php namespace App\Models;

use CodeIgniter\Model;
// use Config\Database;

class BlogModel extends Model
{
    // protected $db;
    // public function __construct()
    // {
    //     $this->db = db_connect();
    // }

    protected $table      = 'posts';
    protected $primaryKey = 'post_id';

    // protected $returnType     = 'array';
    // protected $useSoftDeletes = true;

    protected $allowedFields = ['post_title', 'post_content'];

    protected $useTimestamps = true;
    protected $createdField  = 'post_created_at';
    protected $updatedField  = 'post_updated_at';
    // protected $deletedField  = 'deleted_at';

    // protected $validationRules    = [];
    // protected $validationMessages = [];
    // protected $skipValidation     = false;

    protected $beforeInsert = ['checkTitle']; //codeigniter will check this methodds before insert
    
    public function checkTitle(array $data)
    {
        $newTitle = $data['data']['post_title'].' Blog Post Title';
        $data['data']['post_title'] = $newTitle;
        return $data;
    }
    
    public function getPosts(){
        $builder = $this->table($this->table);
        $builder->join('users', 'posts.post_author = users.user_id');
        $posts = $builder->get()->getResult();
        return $posts;
    }
}