<?php $this->extend('layouts/main'); ?>

<?php $this->section('content'); ?>

<h1><?= $page_header; ?></h1>
<!-- showing success message -->
<?php if(isset($success_message) && ($success_message !== '')) { ?>
    <div class="col-md-12 alert alert-success alert-dismissible fade show" role="alert">
        <?= $success_message; ?>
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
<?php } ?>

<div class="row">
    <?php if(isset($validation) && ($validation->listErrors() !== '')) { ?>
    <!-- <div class="col-md-12 alert alert-warning alert-dismissible fade show" role="alert">
        <?php //echo $validation->listErrors(); ?>
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div> -->
    <?php } ?>
    <form method="POST" action="/user/new" class="col-md-12" enctype="multipart/form-data">
        <div class="mb-3">
            <label for="UserName" class="form-label">Name</label>
            <input type="text" class="form-control" id="UserName" name="name" placeholder="Name">
            <?php if(isset($validation) && ($validation->hasError('name'))){ echo "<span class='alert text-danger'><small>".$validation->getError('name')."</small></span>"; }?>
        </div>
        <div class="mb-3">
            <label for="UserEmail" class="form-label">Email</label>
            <input type="text" class="form-control" id="UserEmail" name="email" placeholder="Email">
            <?php if(isset($validation) && ($validation->hasError('email'))){ echo "<span class='alert text-danger'><small>".$validation->getError('email')."</small></span>"; }?>
        </div>
        <div class="mb-3">
            <label for="UserDoB" class="form-label">Date of Birth</label>
            <input type="date" class="form-control" id="UserDoB" name="date_of_birth" placeholder="Date of Birth">
            <?php if(isset($validation) && ($validation->hasError('date_of_birth'))){ echo "<span class='alert text-danger'><small>".$validation->getError('date_of_birth')."</small></span>"; }?>
        </div>
        <div class="mb-3">
            <label for="UserPass" class="form-label">Password</label>
            <input type="password" class="form-control" id="UserPass" name="password" placeholder="Password">
            <?php if(isset($validation) && ($validation->hasError('password'))){ echo "<span class='alert text-danger'><small>".$validation->getError('password')."</small></span>"; }?>
        </div>
        <div class="mb-3">
            <label for="ConfirmUserPass" class="form-label">Confirm Password</label>
            <input type="password" class="form-control" id="ConfirmUserPass" name="confirm_password"
                placeholder="Confirm Password">
            <?php if(isset($validation) && ($validation->hasError('confirm_password'))){ echo "<span class='alert text-danger'><small>".$validation->getError('confirm_password')."</small></span>"; }?>
        </div>
        <div class="mb-3">
            <label for="UserCategory">Category</label>
            <select class="form-control" id="UserCategory" name="category">
                <option value="">Choose...</option>
                <?php
                    foreach($categories as $category):
                ?>
                <option value="<?=$category;?>"><?=$category;?></option>
                <?php endforeach;
                ?>
            </select>
            <?php if(isset($validation) && ($validation->hasError('category'))){ echo "<span class='alert text-danger'><small>".$validation->getError('category')."</small></span>"; }?>
        </div>        
        <div class="form-group">
            <label for="profilePicture">Profile Picture</label>
            <input type="file" class="form-control-file" id="profilePicture" name="profile_picture">
            <?php if(isset($validation) && ($validation->hasError('profile_picture'))){ echo "<span class='alert text-danger'><small>".$validation->getError('profile_picture')."</small></span>"; }?>
        </div>
        <button type="submit" class="btn btn-primary">Create</button>
    </form>
</div>
<?php $this->endSection('content'); ?>
<script>
    // Add the following code if you want the name of the file appear on select
    // $(".custom-file-input").on("change", function () {
    //     var fileName = $(this).val().split("\\").pop();
    //     $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
    // });
</script>