<?php $this->extend('layouts/main'); ?>

<?php $this->section('content'); ?>

<h1><?= $page_header; ?></h1>

<div class="row row-cols-1 row-cols-md-2 g-4">
    <div class="col-md-6">
        <div class="card">
            <img src="/assets/images/lufi.jpeg" class="card-img-top" alt="...">
            <div class="card-body">
                <h5 class="card-title"><?= $post_title; ?></h5>
                <p class="card-text"><?= $post_content; ?></p>
                <?php
                    if(isset($post_id) && !empty($post_id)){
                ?>
                <a href="/blog/edit/<?= $post_id; ?>" class="btn btn-primary">Edit</a>
                <a href="/blog/delete/<?= $post_id; ?>" class="btn btn-danger">Delete</a>
                <?php
                    }
                ?>
            </div>
        </div>
    </div>       
</div>
<?php $this->endSection('content'); ?>