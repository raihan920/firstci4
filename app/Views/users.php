<?php $this->extend('layouts/main'); ?>

<?php $this->section('content'); ?>

<h1><?= $page_header; ?></h1>

<div class="row">
<?php
    foreach ($users as $user) :
        echo view_cell('\App\Libraries\User::printItem', ['user' => $user]);
    endforeach;
?>
</div>
<?php $this->endSection('content'); ?>