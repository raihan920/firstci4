<?php $this->extend('layouts/main'); ?>

<?php $this->section('content'); ?>

<h1><?= $page_header; ?></h1>

<div class="row row-cols-1 row-cols-md-2 g-4">
    <div class="col-md-7 offset-md-2">
        <div class="card">
            <img src="<?='../../'.$profile_picture;?>" class="card-img-top" alt="<?= $profile_picture; ?>">
            <div class="card-body">
                <h5 class="card-title"> <b>Name: </b> <?= $name; ?></h5>
                <p class="card-text"> <b>Email: </b> <?= $email; ?></p>
                <p class="card-text"> <b>Date of Birth: </b> <?= $date_of_birth; ?></p>
                <p class="card-text"> <b>Category: </b> <?= $category; ?></p>
                <p class="card-text"> <b>Created at: </b> <?= $user_created_at; ?></p>
                <p class="card-text"> <b>Updated at: </b> <?= $user_updated_at; ?></p>
                <?php
                    if(isset($user_id) && !empty($user_id)){
                ?>
                <a href="/user/edit/<?php //echo $post_id; ?>" class="btn btn-primary">Edit</a>
                <a href="/user/delete/<?php //echo $post_id; ?>" class="btn btn-danger">Delete</a>
                <?php
                    }
                ?>
            </div>
        </div>
    </div>       
</div>
<?php $this->endSection('content'); ?>