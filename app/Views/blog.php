<?php $this->extend('layouts/main'); ?>

<?php $this->section('content'); ?>

<h1><?= $page_header; ?></h1>

<div class="row">
<?php
    foreach ($posts as $post) :
        echo view_cell('\App\Libraries\Blog::printItem', ['post' => $post]);
    endforeach;
?>
</div>
<?php $this->endSection('content'); ?>