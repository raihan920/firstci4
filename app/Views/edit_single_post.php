<?php $this->extend('layouts/main'); ?>

<?php $this->section('content'); ?>

<h1><?= $page_header; ?></h1>

<div class="row">
    <form method="POST" action="/blog/update" class="col-md-12">
        <div class="mb-3 col-md-12">
            <label for="formGroupExampleInput" class="form-label">Title</label>
            <input type="text" class="form-control" id="formGroupExampleInput" name="post_title" placeholder="Title" value="<?= $post_title; ?>">
        </div>
        <div class="mb-3 col-md-12">
            <label for="floatingTextarea2" class="form-label">Content</label>
            <textarea class="form-control" name="post_content" id="floatingTextarea2" style="min-height: 120px;"><?= $post_content; ?></textarea>
        </div>
        <input type="hidden" name="post_id" value="<?= $post_id; ?>">
        <div class="col-12">
            <button type="submit" class="btn btn-success">Update</button>
        </div>
    </form>
</div>
<?php $this->endSection('content'); ?>

