<?php $this->extend('layouts/main'); ?>

<?php $this->section('content'); ?>

<h1><?= $page_header; ?></h1>

<div class="row">
    <?php if(isset($validation) && ($validation->listErrors() !== '')) { ?>
    <div class="col-md-12 alert alert-warning alert-dismissible fade show" role="alert">
        <?= $validation->listErrors(); ?> 
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    <?php } ?>
    <form method="POST" action="/blog/new" class="col-md-12">
        <div class="mb-3">
            <label for="formGroupExampleInput" class="form-label">Title</label>
            <input type="text" class="form-control" id="formGroupExampleInput" name="post_title" placeholder="Title">
        </div>
        <div class="mb-3">
            <label for="floatingTextarea2" class="form-label">Content</label>
            <textarea class="form-control" name="post_content" placeholder="Leave a comment here" id="floatingTextarea2"
                style="height: 120px"></textarea>
        </div>
     
        <button type="submit" class="btn btn-primary">Create</button>

    </form>
</div>
<?php $this->endSection('content'); ?>