<div class="col-md-4 my-2">
    <div class="card">
    <img src="<?= $user->profile_picture_thumbs; ?>" class="card-img-top" alt="Default Image">        
        <div class="card-body">
            <h5 class="card-title"><b>User ID: </b><?= $user->user_id; ?></h5>
            <p class="card-text"><b>User Name: </b><?= $user->name; ?></p>
            <p class="card-text"><b>Category: </b><?= $user->category; ?></p>
            <p class="card-text"><b>Created at: </b><?= $user->user_created_at; ?></p>
            <p class="card-text"><b>Updated at: </b><?= $user->user_updated_at; ?></p>
            <a href="/user/viewSingle/<?= $user->user_id; ?>" class="btn btn-primary">View Full Profile</a>
        </div>
    </div>
</div>