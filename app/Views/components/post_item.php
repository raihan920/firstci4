<div class="col-md-4 my-2">
    <div class="card">
        <img src="/assets/images/lufi.jpeg" class="card-img-top" alt="Default Image">
        <div class="card-body">
            <h5 class="card-title"><?= $post->post_title; ?></h5><!-- because array has a key named 'title' -->
            <p class="card-text"><?= $post->post_content; ?></p>
            <p class="card-text"><b>Created at: </b><?= $post->post_created_at; ?></p>
            <p class="card-text"><b>Author: </b><?= $post->post_author; ?></p>
            <p class="card-text"><b>Updated at: </b><?= $post->post_updated_at; ?></p>
            <p class="card-text"><b>Post no: </b><?= $post->post_id; ?></p>
            <a href="/blog/viewSingle/<?= $post->post_id; ?>" class="btn btn-primary">Read more</a>
        </div>
    </div>
</div>