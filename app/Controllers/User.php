<?php

namespace App\Controllers;

use App\Libraries\Utility;
use App\Models\UserModel;

class User extends BaseController {

    public function index() {

        $allUser = new UserModel();

        $data = [
            "meta_title" => "User List",
            "page_header" => "A list of all of our users"
        ];
        
        $user = $allUser->getUsers();
        $data['users'] = $user; 
        
        return view('users', $data);
        
    }
    
    public function viewSingle($user_id) {        

        $model = new UserModel();
        $post = $model->find($user_id);

        if($post){
            $data = [
                "meta_title" => "Single Page",
                "page_header" => "User Details",
                "name" => $post['name'],
                "email" => $post['email'],
                "date_of_birth" => $post['date_of_birth'],
                "category" => $post['category'],
                "profile_picture" => $post['profile_picture'],
                "user_created_at" => $post['user_created_at'],
                "user_updated_at" => $post['user_updated_at'],
                "user_id" => $post['user_id']                
            ];
        }else{
            $data = [
                "meta_title" => "Single Page",
                "page_header" => "User Details",
                "name" => "Not Found",
                "email" => "Not Found",
                "date_of_birth" => "Not Found",
                "category" => "Not Found",
                "profile_picture" => "",
                "user_created_at" => "Not Found",
                "user_updated_at" => "Not Found",
                "user_id" => "Not Found"  
            ];
        }        
        return view('view_single_user', $data);        
    }
    public function regex_check(array $data)
    {
        $passwordStr = $data['data']['password'];
        // $confirmPasswordStr = $this->input->post('confirm_password');
        if (preg_match("/^(?:'[A-Za-z](([\._\-][A-Za-z0-9])|[A-Za-z0-9])*[a-z0-9_]*')$/", $passwordStr))
        {
            $this->form_validation->set_message('password', 'The %s field is not valid!');
            $this->form_validation->set_message('confirm_password', 'The %s field is not valid!');
            return FALSE;
        }
        else
        {
            return TRUE;
        }
    }

    public function new()
    {
        helper(['form','image']);

        $data = [
            "meta_title" => "New User",
            "page_header" => "Create New User",
            "categories" => [
                'Student',
                'Teacher',
                'Principle'
            ]
        ];

        $rules = [
            'name' => [
                'label'  => 'Name',
                'rules'  => 'trim|required',
                'errors' =>[
                    'required' => 'Name is Required'
                   
                ]
            ],
            'email' => [
                'label'  => 'Email',
                'rules'  => 'trim|required|valid_email|is_unique[users.email]',
                'errors' =>[
                    'required' => 'Email is Required',
                    'valid_email' => 'Not a valid email'
                ]
            ],
            'date_of_birth' => [
                'label' => 'Date of Birth',
                'rules' => 'required'
            ],
            'password' => [              
                'label'  => 'Password',                
                'rules'  => 'trim|required|min_length[8]|regex_match[/(?=.*[\d])(?=.*[A-Z])(?=.*[a-z])(?=.*[!\"#$%&\'()*+,-.\/:;<=>?@\[\]^_`{}~])/]',
                'errors' =>[
                    'required' => 'Password is Required',
                    'min_length' => 'Passwore must be at least 8 characters',                    
                    'regex_match' => 'Password must contain capital letter, small letter, number and special character (!"#$%&\'()*+,-./:;<=>?@[]^_`{}~)'
                ]
            ],
            'confirm_password' => [
                'label'  => 'Confirm Password',
                'rules'  => 'trim|required|min_length[8]|matches[password]|regex_match[/(?=.*[\d])(?=.*[A-Z])(?=.*[a-z])(?=.*[!\"#$%&\'()*+,-.\/:;<=>?@\[\]^_`{}~])/]',
                'errors' =>[
                    'required' => 'Password confirmation is Required',
                    'min_length' => 'Passwore must be at least 8 characters',
                    'matches' => 'Password and Cofirm password are not same',
                    'regex_match' => 'Confirm Password must contain capital letter, small letter, number and special character (!"#$%&\'()*+,-./:;<=>?@[]^_`{}~)'
                ]
            ],
            'category' => [
                'label'  => 'Category',
                'rules'  => 'trim|required',                
                'errors' =>[
                    'required' => 'Category is Required'
                ]
            ],
            'profile_picture' => [
                'label'  => 'Profile Picture',
                'rules'  => 'uploaded[profile_picture]|max_size[profile_picture, 1024]|is_image[profile_picture]' //ext_in[profile_picture,png,jpg]
            ]

        ];
        
        $image = service('image'); //for image manipulation

        if($this->request->getMethod() == 'post'){
            if ($this->validate($rules)) {
                $file = $this->request->getFile('profile_picture');
                if($file->isValid() && !$file->hasMoved()){
                    
                    $file->move('./uploads/profile-picture', $this->request->getPost('name') . '.' . $file->getExtension());
                    //Utility::dd(base_url().$file->getTempName().$file->getName());
                    $_POST['profile_picture'] = $file->getTempName().$file->getName(); //for database

                    // $filePath = $file->getTempName().$file->getName();
                    $fileName = $file->getName();
                    $fileNameWithPath = createProfileImageName($fileName);

                    // //if directory doesnot exist create it
                    // if(!file_exists('./uploads/profile-picture/thumbs/')){
                    //     mkdir('./uploads/profile-picture/thumbs/', 755);
                    // }

                    //make it 150 by 150 px
                    $image->withFile($fileNameWithPath)
                        ->fit(180,150,'center')
                        ->save(createProfileImageName($fileName, 'thumbs'));

                    $_POST['profile_picture_thumbs'] = createProfileImageName($fileName, 'thumbs'); //for database                    
                }
                $model = new UserModel();
                $model->save($_POST);
                $data['success_message'] = "Data has been successfully stored";
            }else{
                $data['validation'] = $this->validator;
            }
        }
        return view('new_user', $data);   
    }

    // public function delete($id){
    //     $model = new BlogModel();
    //     $post = $model->find($id);
    //     if($post){
    //         $model->delete($id);
    //         return redirect()->to('/blog');
    //     }
    // }

    // public function edit($id){
    //     $model = new BlogModel();
    //     $post = $model->find($id);
    //     if($post){
    //         $data = [
    //             "meta_title" => "Single Page",
    //             "page_header" => "This is Blog Single Page",
    //             "post_title" => $post['post_title'],
    //             "post_content" => $post['post_content'],
    //             "post_id" => $post['post_id']
    //         ];
    //     }
    //     return view('edit_single_post', $data);
    // }

    // public function update(){
    //     if($this->request->getMethod() == 'post'){
    //         $model = new BlogModel();
    //         $model->save($_POST);            
    //     }
    //     return $this->viewSingle($_POST['post_id']);
    // }

}