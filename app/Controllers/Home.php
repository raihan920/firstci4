<?php

namespace App\Controllers;

use App\Controllers\Admin\Shop As AdminShop;

class Home extends BaseController {

    public function index() {
        return view('welcome_message');
    }

    public function validateShop() {
        $shop = new Shop();
        echo $shop->product('Laptop', 'HP');
        echo '<br/>';
        $adminShop = new AdminShop();
        echo $adminShop->product();
    }

}
