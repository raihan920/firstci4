<?php

namespace App\Controllers;

use App\Models\BlogModel;

class Blog extends BaseController {

    public function index() {

        $allPost = new BlogModel();
        // echo '<pre>';
        // print_r($allPost->getPosts()); 
        // echo '</pre>';z

        $data = [
            "meta_title" => "Blog Home",
            "page_header" => "This is Blog Page Home"
        ];
        
        //$posts = ['Title 1', 'Title 2', 'Title 3'];
        $posts = $allPost->getPosts();
        $data['posts'] = $posts; 
        
        return view('blog', $data);
        
    }
    
    public function viewSingle($post_id) {        

        $model = new BlogModel();
        $post = $model->find($post_id);

        if($post){
            $data = [
                "meta_title" => "Single Page",
                "page_header" => "This is Blog Single Page",
                "post_title" => $post['post_title'],
                "post_content" => $post['post_content'],
                "post_created_at" => $post['post_created_at'],
                "post_author" => $post['post_author'],
                "post_updated_at" => $post['post_updated_at'],
                "post_id" => $post['post_id']                
            ];
        }else{
            $data = [
                "meta_title" => "Single Page",
                "page_header" => "This is Blog Single Page",
                "post_title" => "Title Not Found",
                "post_content" => "Content Not Found",
                "post_created_at" => "Not Found",
                "post_author" => "Not Found",
                "post_updated_at" => "Not Found",
                "post_id" => "Not Found"   
            ];
        }
        
        return view('view_single_post', $data);        
    }

    public function new()
    {
        helper(['form']);

        $rules = [
            'post_title' => [
                'rules'  => 'required',
                'errors' =>[
                    'required' => 'Post Title is Required'
                ]
            ],
            'post_content' => [
                'rules'  => 'required',
                'errors' =>[
                    'required' => 'Post Content is Required'
                ]
            ],
        ];

        $data = [
            "meta_title" => "New Post",
            "page_header" => "Create New Post"
        ];

        if($this->request->getMethod() == 'post'){
            if ($this->validate($rules)) {
                $model = new BlogModel();
                $model->save($_POST);
            }else{
                $data['validation'] = $this->validator;
            }

        }
        return view('new_post', $data);   
    }

    public function delete($id){
        $model = new BlogModel();
        $post = $model->find($id);
        if($post){
            $model->delete($id);
            return redirect()->to('/blog');
        }
    }

    public function edit($id){
        $model = new BlogModel();
        $post = $model->find($id);
        if($post){
            $data = [
                "meta_title" => "Single Page",
                "page_header" => "This is Blog Single Page",
                "post_title" => $post['post_title'],
                "post_content" => $post['post_content'],
                "post_id" => $post['post_id']
            ];
        }
        return view('edit_single_post', $data);
    }

    public function update(){
        if($this->request->getMethod() == 'post'){
            $model = new BlogModel();
            $model->save($_POST);            
        }
        return $this->viewSingle($_POST['post_id']);
    }

}
