-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 06, 2021 at 06:36 PM
-- Server version: 10.4.16-MariaDB
-- PHP Version: 7.4.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `firstci4`
--

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE `posts` (
  `post_id` int(11) NOT NULL,
  `post_title` varchar(255) NOT NULL,
  `post_content` text NOT NULL,
  `post_created_at` datetime NOT NULL,
  `post_author` int(11) NOT NULL,
  `post_updated_at` datetime NOT NULL,
  `post_deleted_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `posts`
--

INSERT INTO `posts` (`post_id`, `post_title`, `post_content`, `post_created_at`, `post_author`, `post_updated_at`, `post_deleted_at`) VALUES
(1, 'Quia consequatur officiis consectetur id corporis atque.', 'I can\'t see you?\' She was a most extraordinary noise going on between the executioner, the King, \'that only makes the world you fly, Like a tea-tray in the direction it pointed to, without trying to.', '2008-09-25 23:58:04', 1, '1988-12-31 21:11:11', '2001-08-10 17:46:05'),
(2, 'Tempora est aperiam eius perspiciatis repudiandae sed error.', 'It sounded an excellent plan, no doubt, and very soon finished it off. \'If everybody minded their own business!\' \'Ah, well! It means much the most confusing thing I ask! It\'s always six o\'clock.', '1989-12-07 19:31:34', 2, '1996-01-08 08:45:41', '1998-01-29 20:03:43'),
(3, 'Qui non ducimus aliquid et eveniet.', 'I wonder?\' And here Alice began telling them her adventures from the time they were lying on the shingle--will you come and join the dance? \"You can really have no idea how confusing it is to find.', '2002-03-27 08:12:42', 3, '1987-12-05 20:28:58', '2017-03-14 14:39:52'),
(4, 'Nostrum voluptatem beatae maxime ut dolorum est minima.', 'Pennyworth only of beautiful Soup? Beau--ootiful Soo--oop! Soo--oop of the Lobster Quadrille, that she ought not to be two people! Why, there\'s hardly enough of me left to make out at the sides of.', '1995-06-16 11:48:55', 4, '1988-05-30 22:07:27', '1979-11-28 15:03:36'),
(5, 'Commodi qui accusamus totam necessitatibus sint suscipit.', 'By the use of repeating all that green stuff be?\' said Alice. \'Of course not,\' Alice cautiously replied: \'but I must sugar my hair.\" As a duck with its tongue hanging out of it, and then another.', '2018-06-30 04:24:16', 5, '1986-02-11 15:53:59', '2003-05-21 10:33:38'),
(6, 'Deleniti ducimus ut ea sint et nesciunt excepturi quo.', 'Gryphon. \'We can do without lobsters, you know. Which shall sing?\' \'Oh, YOU sing,\' said the Lory. Alice replied very gravely. \'What else have you got in your pocket?\' he went on at last, they must.', '1982-05-30 15:56:57', 6, '1973-06-10 08:53:20', '1980-12-04 19:24:26'),
(7, 'Nam et et laborum fugit voluptate unde.', 'Oh, my dear Dinah! I wonder what I say,\' the Mock Turtle sighed deeply, and drew the back of one flapper across his eyes. \'I wasn\'t asleep,\' he said to live. \'I\'ve seen a cat without a porpoise.\'.', '2020-01-25 08:51:16', 7, '2020-01-12 17:56:34', '2018-04-30 19:25:20'),
(8, 'Laborum ducimus dolores cum rem a.', 'Alice could see, when she next peeped out the Fish-Footman was gone, and the other paw, \'lives a March Hare. Alice was so ordered about by mice and rabbits. I almost think I can remember feeling a.', '2020-04-07 07:53:22', 8, '1974-07-05 10:48:09', '2018-04-16 20:13:34'),
(9, 'Title Edited', 'Content Edited Lufi yoyo', '1983-02-21 13:16:09', 9, '2020-12-19 00:51:38', '1996-09-11 14:46:14'),
(10, 'Expedita explicabo reprehenderit neque earum mollitia inventore porro laudantium.', 'He says it kills all the way down one side and then she had known them all her riper years, the simple rules their friends had taught them: such as, that a moment\'s pause. The only things in the.', '1994-11-16 08:43:06', 10, '1979-06-14 03:41:57', '1988-06-16 20:55:56'),
(11, 'Voluptate dolorum fugiat sapiente et consequatur numquam et.', 'It doesn\'t look like it?\' he said, \'on and off, for days and days.\' \'But what did the Dormouse shall!\' they both cried. \'Wake up, Dormouse!\' And they pinched it on both sides of the bill, \"French,.', '2005-02-07 13:32:05', 11, '1982-12-03 18:01:50', '1979-11-03 13:04:55'),
(12, 'Rem rerum enim ad pariatur cum.', 'I gave her answer. \'They\'re done with a great deal of thought, and rightly too, that very few things indeed were really impossible. There seemed to be said. At last the Mouse, in a minute or two,.', '2013-11-28 05:01:32', 12, '1978-09-11 03:13:33', '1997-10-07 07:29:16'),
(13, 'Numquam odit earum quia debitis voluptates inventore dolores.', 'Alice could only see her. She is such a nice soft thing to nurse--and she\'s such a neck as that! No, no! You\'re a serpent; and there\'s no use going back to my boy, I beat him when he finds out who.', '1998-02-01 07:26:14', 13, '2007-12-06 15:51:35', '1987-08-04 09:56:39'),
(14, 'Ex sunt sint necessitatibus earum sunt.', 'There was a large pigeon had flown into her face. \'Wake up, Dormouse!\' And they pinched it on both sides of the cupboards as she could not join the dance. Will you, won\'t you, will you, old fellow?\'.', '2002-06-10 21:18:45', 14, '1992-04-20 16:44:52', '1997-12-19 05:57:35'),
(15, 'Voluptatum et provident illo fugiat reiciendis eveniet facere.', 'He looked at the bottom of the Lobster; I heard him declare, \"You have baked me too brown, I must have been changed in the act of crawling away: besides all this, there was silence for some way of.', '1989-06-19 00:57:16', 15, '1987-03-06 21:21:09', '2015-12-04 01:22:06'),
(16, 'Excepturi in nihil vel repellendus dicta aut.', 'Majesty,\' said Two, in a mournful tone, \'he won\'t do a thing before, but she could not swim. He sent them word I had our Dinah here, I know all the arches are gone from this morning,\' said Alice.', '1996-10-15 04:40:21', 16, '1976-07-06 03:32:21', '1977-05-27 23:12:01'),
(17, 'Minus laudantium sit et assumenda harum molestias dolore sunt.', 'Alice turned and came back again. \'Keep your temper,\' said the Mock Turtle said: \'I\'m too stiff. And the moral of that is--\"Oh, \'tis love, \'tis love, that makes you forget to talk. I can\'t tell you.', '1995-12-20 00:51:19', 17, '1971-01-05 21:54:36', '1973-06-30 01:42:13'),
(18, 'Aut sint commodi facere quia ut.', 'There\'s no pleasing them!\' Alice was only too glad to find quite a chorus of \'There goes Bill!\' then the Mock Turtle replied in a coaxing tone, and she went on in a very pretty dance,\' said Alice.', '1989-01-01 18:02:16', 18, '2005-06-11 03:52:52', '1985-09-02 05:16:38'),
(19, 'Voluptas quae quo quis est et nobis sed.', 'However, this bottle does. I do so like that curious song about the right way to change them--\' when she looked up, and there stood the Queen added to one of them can explain it,\' said Alice, very.', '1981-02-11 11:47:02', 19, '2018-12-06 03:37:46', '1979-08-11 13:32:59'),
(20, 'Velit soluta dignissimos praesentium.', 'Pepper For a minute or two, it was written to nobody, which isn\'t usual, you know.\' He was an immense length of neck, which seemed to have wondered at this, but at last the Gryphon said, in a voice.', '2000-04-26 17:07:56', 20, '2011-11-23 02:31:28', '2004-11-23 08:56:29'),
(21, 'Adipisci accusamus adipisci culpa hic repellat.', 'I hate cats and dogs.\' It was the BEST butter, you know.\' \'I don\'t much care where--\' said Alice. \'That\'s very important,\' the King eagerly, and he called the Queen, and Alice heard the King.', '2011-11-07 16:30:00', 1, '2005-02-26 03:21:06', '1986-05-17 21:41:29'),
(22, 'Nesciunt aut distinctio illo mollitia vitae distinctio.', 'New Zealand or Australia?\' (and she tried the little magic bottle had now had its full effect, and she felt that it was her turn or not. \'Oh, PLEASE mind what you\'re talking about,\' said Alice..', '1980-11-15 00:42:59', 2, '1986-12-22 01:51:05', '1990-11-05 19:53:15'),
(23, 'Qui saepe provident in ut et.', 'YOUR adventures.\' \'I could tell you my history, and you\'ll understand why it is right?\' \'In my youth,\' said his father, \'I took to the Caterpillar, and the Gryphon said, in a bit.\' \'Perhaps it.', '2008-12-04 22:03:48', 3, '2009-11-14 18:33:39', '1988-09-28 06:34:01'),
(24, 'Repudiandae ullam odio vel nisi sapiente modi.', 'I only wish they COULD! I\'m sure she\'s the best of educations--in fact, we went to school in the sea, some children digging in the air. This time Alice waited till the Pigeon had finished. \'As if I.', '1974-06-22 14:05:19', 4, '2013-10-04 23:30:56', '1982-02-26 13:27:21'),
(25, 'Delectus nesciunt optio officiis consequatur corrupti.', 'Five, \'and I\'ll tell him--it was for bringing the cook tulip-roots instead of the jurymen. \'No, they\'re not,\' said the King and Queen of Hearts, who only bowed and smiled in reply. \'Please come back.', '1970-07-04 10:18:35', 5, '2017-03-06 02:54:37', '1977-05-21 11:30:14'),
(26, 'At non rerum odio est.', 'Gryphon at the house, and wondering what to uglify is, you know. But do cats eat bats, I wonder?\' As she said to herself that perhaps it was getting quite crowded with the clock. For instance, if.', '2002-07-25 01:43:59', 6, '1999-10-02 16:18:18', '1971-10-30 01:45:58'),
(27, 'Nemo minus ut laudantium repellendus nulla numquam praesentium.', 'CHAPTER XII. Alice\'s Evidence \'Here!\' cried Alice, quite forgetting that she never knew so much into the earth. Let me see--how IS it to be otherwise than what you mean,\' said Alice. The poor little.', '1974-02-19 16:23:40', 7, '1975-10-22 21:12:34', '2020-07-09 02:54:35'),
(28, 'Exercitationem placeat aliquam provident voluptatibus excepturi eos.', 'I am in the distance. \'Come on!\' and ran till she had accidentally upset the week before. \'Oh, I beg your pardon,\' said Alice in a tone of great relief. \'Now at OURS they had settled down again, the.', '1982-01-06 01:46:18', 8, '2000-11-05 02:17:46', '2016-06-22 00:23:23'),
(29, 'Sint nostrum doloremque minus quo illum iste minus.', 'Lory, who at last turned sulky, and would only say, \'I am older than I am so VERY tired of this. I vote the young Crab, a little animal (she couldn\'t guess of what sort it was) scratching and.', '2001-08-20 23:13:52', 9, '1984-08-22 06:43:53', '1975-06-17 12:21:24'),
(30, 'Voluptas et excepturi alias libero.', 'They all sat down with wonder at the righthand bit again, and we put a white one in by mistake; and if the Mock Turtle\'s Story \'You can\'t think how glad I am now? That\'ll be a footman because he.', '2002-10-08 10:36:01', 10, '1974-05-05 18:52:53', '2015-02-09 00:52:33'),
(31, 'Consequatur dolores necessitatibus repellat molestias.', 'I\'m sure _I_ shan\'t be beheaded!\' said Alice, in a hurry to change the subject. \'Ten hours the first minute or two. \'They couldn\'t have done just as I\'d taken the highest tree in the court!\' and the.', '1983-10-29 22:15:06', 11, '1994-03-30 11:06:21', '1972-03-30 04:00:45'),
(32, 'Reprehenderit repellendus est maiores cupiditate eaque vero.', 'Father William,\' the young Crab, a little girl she\'ll think me for a little feeble, squeaking voice, (\'That\'s Bill,\' thought Alice,) \'Well, I never knew so much already, that it was quite a.', '1971-06-05 13:35:59', 12, '1976-05-03 01:30:58', '2017-03-02 01:15:31'),
(33, 'Perspiciatis quam quo cupiditate quas accusantium aliquam rerum.', 'King, and the others looked round also, and all the first to speak. \'What size do you know why it\'s called a whiting?\' \'I never saw one, or heard of uglifying!\' it exclaimed. \'You know what it was.', '2003-09-12 14:18:35', 13, '1985-03-05 02:15:42', '1983-03-23 19:36:09'),
(34, 'Voluptas nostrum est iste ipsam sit incidunt.', 'Alice remained looking thoughtfully at the other side of the legs of the month is it?\' The Gryphon sat up and to stand on their slates, and she soon made out what she did, she picked her way into a.', '2017-12-27 11:48:58', 14, '1992-06-12 02:31:12', '1990-01-31 08:02:46'),
(35, 'Hic quisquam adipisci pariatur nesciunt.', 'Two began in a long, low hall, which was sitting on the Duchess\'s voice died away, even in the last time she found herself in a more subdued tone, and added \'It isn\'t directed at all,\' said the.', '1999-01-01 04:52:35', 15, '1970-09-03 04:37:20', '1978-10-17 18:27:43'),
(36, 'Et molestias atque nulla illum laborum.', 'Cat again, sitting on a crimson velvet cushion; and, last of all her wonderful Adventures, till she heard the Rabbit hastily interrupted. \'There\'s a great many more than three.\' \'Your hair wants.', '1993-04-05 15:59:30', 16, '1976-09-26 22:35:18', '1983-05-22 07:14:23'),
(37, 'Id maiores sint eveniet adipisci ut corporis modi.', 'CHAPTER III. A Caucus-Race and a large pool all round the thistle again; then the other, trying every door, she found her head made her look up and repeat something now. Tell her to speak again. The.', '1986-06-12 06:24:06', 17, '1991-09-03 22:24:25', '1974-09-23 04:22:56'),
(38, 'Repellat dicta veniam aut doloremque ab quasi quas ad.', 'Caterpillar. \'I\'m afraid I can\'t get out again. That\'s all.\' \'Thank you,\' said Alice, a good deal worse off than before, as the Caterpillar called after it; and while she was quite out of court!.', '1984-11-10 19:41:18', 18, '1985-08-18 14:25:15', '2008-06-12 13:11:14'),
(39, 'Deserunt enim sint ipsum officia quo laborum.', 'Duchess said to herself, and nibbled a little of her own mind (as well as she did not dare to disobey, though she knew that it might appear to others that what you like,\' said the Caterpillar. Alice.', '1987-07-27 15:54:15', 19, '1984-11-16 23:39:52', '2000-07-14 23:39:35'),
(40, 'Labore aut ex aut totam quasi eaque distinctio.', 'So they got thrown out to sea as you liked.\' \'Is that all?\' said Alice, \'and why it is to find herself still in sight, and no more to do this, so she went down to the other end of the suppressed.', '2016-11-15 20:09:52', 20, '2007-06-04 12:45:48', '1973-11-20 18:07:58'),
(41, 'Omnis et est voluptates.', 'I shall remember it in a VERY good opportunity for showing off a little bird as soon as she had found the fan and gloves, and, as there was no label this time the Queen furiously, throwing an.', '1971-05-01 12:08:01', 1, '1984-06-16 21:02:51', '1975-08-10 01:55:01'),
(42, 'Fugit sunt harum eum quam nihil alias et.', 'Has lasted the rest of my own. I\'m a hatter.\' Here the other queer noises, would change to tinkling sheep-bells, and the poor little feet, I wonder if I shall have some fun now!\' thought Alice. \'I\'m.', '1995-09-02 19:51:53', 2, '2010-03-25 02:34:41', '1973-10-22 05:51:17'),
(43, 'Consequatur iure porro perspiciatis totam facere ut.', 'This time there were TWO little shrieks, and more faintly came, carried on the end of half those long words, and, what\'s more, I don\'t want to get in?\' \'There might be some sense in your pocket?\' he.', '2002-01-11 16:14:21', 3, '1983-02-25 10:14:36', '2006-07-12 08:13:13'),
(44, 'Totam eius nam explicabo consectetur sed quis.', 'Then it got down off the cake. * * * * * \'Come, my head\'s free at last!\' said Alice very humbly: \'you had got to the Cheshire Cat: now I shall think nothing of tumbling down stairs! How brave.', '2014-12-03 09:34:58', 4, '1972-04-27 08:57:07', '1988-07-21 15:04:25'),
(45, 'Consectetur facere velit soluta iure eos est.', 'After a minute or two she walked sadly down the chimney?--Nay, I shan\'t! YOU do it!--That I won\'t, then!--Bill\'s to go on. \'And so these three weeks!\' \'I\'m very sorry you\'ve been annoyed,\' said.', '1973-10-05 22:57:27', 5, '2009-03-18 07:17:01', '2016-11-20 22:19:01'),
(46, 'Consequuntur molestias quae non quasi quod dicta.', 'Dormouse sulkily remarked, \'If you knew Time as well she might, what a long silence after this, and Alice was only sobbing,\' she thought, and it said in a tone of delight, and rushed at the door of.', '2005-07-08 18:52:09', 6, '2001-05-30 07:56:45', '1993-11-01 14:28:19'),
(47, 'Ipsam facilis aliquid nisi expedita quam.', 'WOULD twist itself round and round goes the clock in a Little Bill It was as much as serpents do, you know.\' Alice had been found and handed back to my jaw, Has lasted the rest of the trees as well.', '2005-05-12 19:43:57', 7, '1974-02-15 23:42:22', '1981-08-11 08:06:42'),
(48, 'Perspiciatis suscipit temporibus nihil iste sint.', 'Dormouse, not choosing to notice this last word two or three times over to the jury, who instantly made a dreadfully ugly child: but it puzzled her a good opportunity for making her escape; so she.', '1970-08-04 16:49:50', 8, '2004-02-25 01:50:34', '2007-07-12 18:25:38'),
(49, 'Enim debitis quis voluptatum laboriosam qui sed.', 'That WILL be a comfort, one way--never to be lost: away went Alice like the three gardeners, oblong and flat, with their heads!\' and the little thing sat down a jar from one of the treat. When the.', '1989-04-09 06:28:10', 9, '2007-08-10 21:39:23', '1972-02-19 03:23:17'),
(50, 'Modi amet totam iusto voluptas ut sunt exercitationem.', 'I\'m never sure what I\'m going to give the hedgehog to, and, as the rest of the thing yourself, some winter day, I will tell you my adventures--beginning from this side of the tea--\' \'The twinkling.', '1990-03-05 05:07:16', 10, '2011-08-05 12:13:26', '2014-08-28 11:36:52'),
(51, 'Doloribus magni aut ut ab qui aspernatur.', 'I\'m not used to call him Tortoise, if he doesn\'t begin.\' But she went on, looking anxiously round to see a little more conversation with her head impatiently; and, turning to the game. CHAPTER IX..', '2006-12-11 05:38:42', 11, '2012-12-22 09:19:11', '2001-09-18 08:56:12'),
(52, 'Ducimus repellendus quam distinctio.', 'No room!\' they cried out when they hit her; and when she had grown up,\' she said to herself; \'the March Hare said to Alice; and Alice guessed who it was, and, as a boon, Was kindly permitted to.', '1994-09-26 01:31:43', 12, '2012-09-08 17:19:27', '2005-10-27 19:06:43'),
(53, 'Praesentium fugiat vero officia sed vel autem nam.', 'Duchess: you\'d better leave off,\' said the Mock Turtle said: \'I\'m too stiff. And the moral of that is, but I hadn\'t gone down that rabbit-hole--and yet--and yet--it\'s rather curious, you know, this.', '2012-08-03 04:10:06', 13, '1996-03-08 04:25:22', '2008-03-31 08:23:28'),
(54, 'Consequatur soluta id dolore soluta.', 'The Mouse did not like to be otherwise.\"\' \'I think I can creep under the window, and some \'unimportant.\' Alice could not possibly reach it: she could remember them, all these changes are! I\'m never.', '2016-07-29 22:04:47', 14, '1988-03-28 19:57:50', '2002-02-04 06:21:16'),
(55, 'Eum molestias tempore ut ipsam sint quae fugiat.', 'I\'m not Ada,\' she said, \'than waste it in a dreamy sort of chance of getting her hands on her spectacles, and began smoking again. This time there were no arches left, and all her fancy, that: they.', '1996-10-04 08:43:08', 15, '2014-04-08 07:51:10', '2015-09-30 12:40:10'),
(56, 'Aut aspernatur nemo numquam quaerat fugiat molestiae et.', 'Who ever saw in another moment that it signifies much,\' she said this, she noticed a curious appearance in the trial one way of speaking to a shriek, \'and just as if it wasn\'t very civil of you to.', '1984-10-19 20:23:36', 16, '2011-05-26 14:30:19', '1978-07-17 00:10:26'),
(57, 'Facere animi similique libero nisi.', 'Forty-two. ALL PERSONS MORE THAN A MILE HIGH TO LEAVE THE COURT.\' Everybody looked at them with one finger, as he shook both his shoes off. \'Give your evidence,\' said the youth, \'and your jaws are.', '1994-12-02 10:26:59', 17, '2019-10-07 21:32:24', '1986-09-08 11:44:48'),
(58, 'Minus odit voluptatem quaerat architecto.', 'Dinah, tell me your history, you know,\' the Hatter began, in a very respectful tone, but frowning and making faces at him as he shook his grey locks, \'I kept all my life, never!\' They had a little.', '2005-12-23 01:43:44', 18, '2018-03-04 07:33:24', '1976-06-30 00:53:12'),
(59, 'Ex et molestiae labore necessitatibus ex.', 'Majesty must cross-examine THIS witness.\' \'Well, if I might venture to say but \'It belongs to the puppy; whereupon the puppy jumped into the loveliest garden you ever saw. How she longed to change.', '1977-06-30 08:45:21', 19, '1973-07-11 17:46:02', '1974-07-21 19:27:46'),
(60, 'Nihil magnam corporis non.', 'Queen, stamping on the second verse of the room. The cook threw a frying-pan after her as she listened, or seemed to be trampled under its feet, \'I move that the mouse doesn\'t get out.\" Only I don\'t.', '1992-07-23 07:43:15', 20, '2015-01-21 08:31:53', '1976-01-12 04:49:04'),
(61, 'Laborum eligendi qui nobis et similique.', 'King very decidedly, and the Gryphon at the cook tulip-roots instead of the Mock Turtle. \'Certainly not!\' said Alice angrily. \'It wasn\'t very civil of you to set them free, Exactly as we needn\'t try.', '1996-12-15 01:14:24', 1, '1981-09-30 21:52:35', '1970-09-26 23:09:07'),
(62, 'Quaerat est quia iure rem reiciendis totam.', 'King said, with a little pattering of feet on the stairs. Alice knew it was sneezing and howling alternately without a cat! It\'s the most curious thing I know. Silence all round, if you hold it too.', '1986-05-13 06:55:22', 2, '1997-11-29 07:58:18', '1994-01-11 01:15:47'),
(63, 'Perspiciatis ipsa ratione sapiente quo.', 'Ann!\' said the Queen. \'Never!\' said the Rabbit in a very short time the Mouse to tell me your history, you know,\' Alice gently remarked; \'they\'d have been a RED rose-tree, and we put a white one in.', '2011-11-12 04:38:10', 3, '2000-02-13 10:29:18', '1987-08-29 22:27:27'),
(64, 'Veniam laborum repellat est autem.', 'Alice said nothing: she had never had fits, my dear, and that he shook his head off outside,\' the Queen said--\' \'Get to your places!\' shouted the Gryphon, and, taking Alice by the pope, was soon.', '2007-07-22 10:09:07', 4, '1991-06-02 05:40:09', '2002-10-13 04:39:23'),
(65, 'Provident voluptate repellendus numquam saepe dolorem.', 'Rabbit began. Alice gave a little of it?\' said the King hastily said, and went on in a low curtain she had not got into it), and handed back to my boy, I beat him when he sneezes; For he can.', '1988-04-30 23:46:24', 5, '2013-05-24 11:11:06', '2000-02-08 17:48:36'),
(66, 'Excepturi ducimus eius ducimus vel veritatis ducimus dicta.', 'Time, and round the rosetree; for, you see, so many lessons to learn! Oh, I shouldn\'t like THAT!\' \'Oh, you foolish Alice!\' she answered herself. \'How can you learn lessons in here? Why, there\'s.', '2016-05-17 14:19:30', 6, '1980-03-09 19:04:33', '2016-05-04 10:01:45'),
(67, 'Ullam nihil molestiae dolore incidunt.', 'And the moral of THAT is--\"Take care of themselves.\"\' \'How fond she is only a pack of cards: the Knave of Hearts, she made out that she looked down into a pig, my dear,\' said Alice, \'it\'s very.', '2002-03-26 22:49:44', 7, '1976-08-28 00:39:33', '1979-04-26 03:48:51'),
(68, 'Ut aut possimus quibusdam rerum ut nostrum.', 'Alice opened the door between us. For instance, suppose it were nine o\'clock in the night? Let me see--how IS it to speak first, \'why your cat grins like that?\' \'It\'s a Cheshire cat,\' said the King.', '1979-11-02 22:52:12', 8, '1970-08-10 04:45:34', '2019-01-15 07:30:15'),
(69, 'Deleniti ipsum qui incidunt nesciunt.', 'I was going on, as she went on eagerly: \'There is such a very interesting dance to watch,\' said Alice, surprised at her hands, and she was about a thousand times as large as himself, and this was.', '1999-09-20 01:00:54', 9, '2009-01-07 11:40:18', '2018-07-10 11:30:12'),
(70, 'Amet ad hic sed vero consequatur.', 'March Hare: she thought it had some kind of thing never happened, and now here I am very tired of being all alone here!\' As she said this, she noticed that the Queen to-day?\' \'I should think very.', '2016-04-09 21:39:57', 10, '1989-06-09 17:58:46', '2020-07-16 20:08:26'),
(71, 'Commodi enim hic corporis possimus vel velit.', 'YOUR shoes done with?\' said the Caterpillar. This was such a noise inside, no one could possibly hear you.\' And certainly there was a most extraordinary noise going on rather better now,\' she said,.', '2016-07-24 15:29:14', 11, '2004-08-21 10:26:03', '2003-03-20 08:43:10'),
(72, 'Quam molestias rerum harum unde iure dolorem.', 'Come on!\' \'Everybody says \"come on!\" here,\' thought Alice, \'and those twelve creatures,\' (she was rather glad there WAS no one to listen to her. \'I wish the creatures wouldn\'t be in Bill\'s place for.', '2001-09-14 02:23:06', 12, '1985-10-11 12:26:04', '1990-09-01 22:10:15'),
(73, 'Minus sit sapiente atque.', 'Never heard of \"Uglification,\"\' Alice ventured to taste it, and then said \'The fourth.\' \'Two days wrong!\' sighed the Lory, who at last it unfolded its arms, took the thimble, saying \'We beg your.', '1980-11-18 06:44:06', 13, '1997-09-06 15:48:26', '1994-05-22 10:40:33'),
(74, 'Dicta placeat a aspernatur qui.', 'Some of the room again, no wonder she felt that there was room for her. \'I can hardly breathe.\' \'I can\'t go no lower,\' said the Hatter; \'so I can\'t be Mabel, for I know who I am! But I\'d better take.', '2014-04-08 07:29:54', 14, '2018-07-19 22:44:31', '2000-06-17 02:11:47'),
(75, 'Debitis sapiente unde temporibus sit modi.', 'Father William replied to his son, \'I feared it might belong to one of its mouth, and addressed her in an offended tone, \'Hm! No accounting for tastes! Sing her \"Turtle Soup,\" will you, won\'t you,.', '2015-03-09 07:35:30', 15, '2000-02-03 06:02:17', '2012-09-19 15:29:20'),
(76, 'Amet sunt corrupti et repudiandae repellat nam est quis.', 'I only knew the meaning of half those long words, and, what\'s more, I don\'t want YOU with us!\"\' \'They were learning to draw,\' the Dormouse crossed the court, she said to herself, \'in my going out.', '1998-05-04 20:55:30', 16, '2017-04-08 08:28:16', '2018-12-23 07:34:21'),
(77, 'Nulla consectetur architecto quis et dolor quia.', 'Father William replied to his son, \'I feared it might belong to one of the others looked round also, and all the jurymen on to the door. \'Call the next thing was waving its right paw round, \'lives a.', '1995-12-27 12:24:04', 17, '1973-12-25 19:01:50', '1981-05-08 12:24:11'),
(78, 'Voluptatem est dicta accusamus voluptatibus quae velit.', 'Bill! I wouldn\'t say anything about it, you know--\' \'What did they live at the cook took the cauldron of soup off the cake. * * \'What a number of executions the Queen till she had gone through that.', '1998-06-15 15:25:10', 18, '2005-07-01 07:03:54', '1991-03-26 10:49:03'),
(79, 'In ipsa optio soluta ullam.', 'Caterpillar The Caterpillar was the Rabbit was still in sight, and no room at all this time, and was gone in a great deal of thought, and looked at Alice. \'I\'M not a mile high,\' said Alice. The poor.', '1996-09-04 13:45:49', 19, '1971-04-23 16:34:34', '1984-01-20 03:28:58'),
(80, 'Assumenda doloremque libero occaecati dolorem aut nesciunt illum.', 'WHAT?\' said the King. (The jury all brightened up at this corner--No, tie \'em together first--they don\'t reach half high enough yet--Oh! they\'ll do well enough; and what does it matter to me whether.', '2000-07-23 05:55:04', 20, '2015-08-12 16:53:10', '1996-02-23 03:47:44'),
(81, 'Nam eos quam ipsa est natus.', 'I do so like that curious song about the temper of your nose-- What made you so awfully clever?\' \'I have answered three questions, and that if something wasn\'t done about it in time,\' said the King,.', '1989-03-13 09:03:22', 1, '1974-11-18 15:57:55', '1970-11-02 22:47:50'),
(82, 'Nihil qui alias incidunt unde.', 'Duchess by this time?\' she said to herself. At this moment Alice appeared, she was now about a whiting to a mouse: she had never had to stop and untwist it. After a while, finding that nothing more.', '1981-08-09 08:33:42', 2, '2011-09-27 21:27:03', '2019-02-25 14:29:56'),
(83, 'Velit mollitia id eos eos.', 'I\'ll set Dinah at you!\' There was nothing else to do, and in despair she put it. She stretched herself up on tiptoe, and peeped over the wig, (look at the door that led into a tidy little room with.', '2020-04-21 00:21:22', 3, '1975-11-12 02:22:21', '1990-03-28 09:58:44'),
(84, 'Ut consequuntur voluptas error dolor.', 'I COULD NOT SWIM--\" you can\'t help it,\' she thought, \'till its ears have come, or at any rate I\'ll never go THERE again!\' said Alice aloud, addressing nobody in particular. \'She\'d soon fetch it.', '1980-02-03 12:31:44', 4, '2007-11-03 19:05:12', '2018-01-18 08:53:54'),
(85, 'Et minus rem voluptas dolores.', 'I get SOMEWHERE,\' Alice added as an unusually large saucepan flew close by her. There was a good deal worse off than before, as the jury wrote it down \'important,\' and some of them even when they.', '2016-11-17 20:38:19', 5, '1999-02-12 15:06:35', '1980-06-12 18:42:01'),
(86, 'Explicabo ipsa ut incidunt deserunt qui laborum recusandae.', 'It was the White Rabbit: it was the matter with it. There was not going to happen next. \'It\'s--it\'s a very curious thing, and she went on for some way, and then unrolled the parchment scroll, and.', '1996-06-20 12:28:06', 6, '1988-07-21 23:20:38', '2018-01-29 04:57:44'),
(87, 'Et et vel blanditiis cum impedit eum sunt.', 'March Hare said--\' \'I didn\'t!\' the March Hare had just begun to dream that she began shrinking directly. As soon as it spoke. \'As wet as ever,\' said Alice a good deal on where you want to be?\' it.', '1978-02-26 22:39:27', 7, '1973-05-22 20:42:01', '1997-06-17 03:51:32'),
(88, 'Corporis ullam nulla commodi magni non voluptatem.', 'I should think!\' (Dinah was the Hatter. This piece of rudeness was more than Alice could bear: she got up, and began to cry again, for this time the Mouse was bristling all over, and she at once.', '2004-08-21 14:57:10', 8, '2013-08-29 22:38:09', '1990-02-23 23:51:02'),
(89, 'Molestiae facere ab illum magnam quia et totam.', 'Latitude was, or Longitude I\'ve got to the Duchess: \'what a clear way you have just been picked up.\' \'What\'s in it?\' said the Gryphon. \'Then, you know,\' said the last few minutes to see some meaning.', '1994-10-15 14:37:01', 9, '1986-07-09 16:27:04', '1996-12-16 13:38:09'),
(90, 'Quia consequatur natus quia est necessitatibus aliquid veritatis ratione.', 'Five! Always lay the blame on others!\' \'YOU\'D better not do that again!\' which produced another dead silence. Alice was rather glad there WAS no one else seemed inclined to say \"HOW DOTH THE LITTLE.', '1992-07-30 19:45:11', 10, '1977-07-05 20:02:32', '2016-04-12 14:57:09'),
(91, 'Nisi minima qui architecto et neque nemo beatae.', 'This time Alice waited a little, \'From the Queen. \'It proves nothing of the way wherever she wanted to send the hedgehog a blow with its arms and frowning at the top of the earth. Let me see: four.', '2017-09-05 21:11:01', 11, '1973-10-27 08:16:13', '2000-05-05 07:33:44'),
(92, 'Ut nemo ut similique enim eaque temporibus aut.', 'Oh! won\'t she be savage if I\'ve kept her eyes anxiously fixed on it, (\'which certainly was not going to be, from one minute to another! However, I\'ve got to the King, and he says it\'s so useful,.', '1997-12-24 23:13:36', 12, '2000-09-17 23:20:39', '1980-07-11 06:49:03'),
(93, 'Fuga dolor nihil et esse pariatur cumque optio.', 'YOU must cross-examine the next moment she appeared on the door and went in. The door led right into a pig, my dear,\' said Alice, looking down at her with large eyes like a candle. I wonder what was.', '2017-10-04 04:35:34', 13, '1997-11-16 04:54:06', '1976-04-24 16:16:56'),
(94, 'Asperiores dolorem laboriosam aperiam omnis dolorem at et minima.', 'Shakespeare, in the pictures of him), while the Mock Turtle to sing you a couple?\' \'You are old,\' said the sage, as he came, \'Oh! the Duchess, the Duchess! Oh! won\'t she be savage if I\'ve kept her.', '1980-08-02 04:03:47', 14, '1972-01-07 04:00:04', '2016-09-25 20:03:24'),
(95, 'Ipsam quasi ut omnis consectetur nesciunt sed.', 'Alice said; \'there\'s a large one, but the Mouse was speaking, and this was of very little use, as it was certainly too much overcome to do it! Oh dear! I wish you wouldn\'t keep appearing and.', '1990-09-05 15:54:53', 15, '1979-01-17 09:05:43', '1996-07-26 22:10:22'),
(96, 'A beatae culpa rerum possimus magnam.', 'Seaography: then Drawling--the Drawling-master was an immense length of neck, which seemed to Alice severely. \'What are tarts made of?\' \'Pepper, mostly,\' said the Gryphon: and Alice called after it;.', '2014-12-04 14:46:59', 16, '1998-02-24 22:58:46', '2005-04-21 02:37:22'),
(97, 'Eum incidunt aliquam cumque incidunt.', 'M?\' said Alice. \'Then you shouldn\'t talk,\' said the Duchess. \'I make you dry enough!\' They all made of solid glass; there was nothing else to do, so Alice ventured to taste it, and then another.', '2000-01-30 22:48:11', 17, '1985-01-20 04:42:29', '2020-08-22 15:11:21'),
(98, 'Qui sapiente iste deleniti eos sapiente earum consequuntur.', 'Pigeon. \'I\'m NOT a serpent!\' said Alice angrily. \'It wasn\'t very civil of you to get her head pressing against the ceiling, and had been to her, one on each side to guard him; and near the right.', '1989-10-29 19:01:38', 18, '2008-01-14 05:59:40', '2003-08-08 20:17:52'),
(99, 'Ab et adipisci iure molestiae exercitationem aliquid qui.', 'Hatter, and, just as usual. \'Come, there\'s half my plan done now! How puzzling all these strange Adventures of hers that you never tasted an egg!\' \'I HAVE tasted eggs, certainly,\' said Alice to.', '1982-05-19 01:56:31', 19, '1992-05-13 08:37:32', '1977-09-29 10:13:40'),
(100, 'Debitis eum incidunt ut culpa vel nulla.', 'King said, turning to the jury, and the shrill voice of the birds and beasts, as well she might, what a dear quiet thing,\' Alice went on at last, they must be off, and had just succeeded in curving.', '1972-10-27 16:31:04', 20, '2013-07-27 02:17:24', '1994-10-06 18:36:56');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `user_id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `email` varchar(255) NOT NULL,
  `date_of_birth` date NOT NULL,
  `password` varchar(255) NOT NULL,
  `category` varchar(10) NOT NULL,
  `profile_picture` varchar(255) NOT NULL,
  `profile_picture_thumbs` varchar(255) NOT NULL,
  `user_created_at` datetime NOT NULL,
  `user_updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `name`, `email`, `date_of_birth`, `password`, `category`, `profile_picture`, `profile_picture_thumbs`, `user_created_at`, `user_updated_at`) VALUES
(1, 'wekikamose', 'jinawefiq@mailinator.com', '2000-03-05', 'f3ed11bbdb94fd9ebdefbaf646ab94d3', 'Teacher', './uploads/profile-picture/wekikamose.jpg', './uploads/profile-picture/thumbs/wekikamose.jpg', '2021-01-06 23:32:41', '2021-01-06 23:32:41'),
(2, 'peqoxy', 'kocetyzic@mailinator.com', '1980-03-19', 'f3ed11bbdb94fd9ebdefbaf646ab94d3', 'Student', './uploads/profile-picture/peqoxy.jpg', './uploads/profile-picture/thumbs/peqoxy.jpg', '2021-01-06 23:33:34', '2021-01-06 23:33:34'),
(3, 'hokobu', 'qekaqev@mailinator.com', '2019-04-26', 'f3ed11bbdb94fd9ebdefbaf646ab94d3', 'Student', './uploads/profile-picture/hokobu.jpg', './uploads/profile-picture/thumbs/hokobu.jpg', '2021-01-06 23:33:54', '2021-01-06 23:33:54'),
(4, 'hysuxicijy', 'popoketi@mailinator.com', '1971-05-04', 'f3ed11bbdb94fd9ebdefbaf646ab94d3', 'Student', './uploads/profile-picture/hysuxicijy.jpg', './uploads/profile-picture/thumbs/hysuxicijy.jpg', '2021-01-06 23:34:07', '2021-01-06 23:34:07');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`post_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
  MODIFY `post_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=101;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
